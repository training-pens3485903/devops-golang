#!/bin/sh

# GitLab API URL
GITLAB_API_URL="https://gitlab.com/api/v4/projects/53896678"

# Website URL to check availability
WEBSITE_URL="172.171.243.186"
LAST_SUCCESS_ENVIRONMENT=$(curl --silent --request GET --header "PRIVATE-TOKEN: $CI_PROJECT_TOKEN" "$GITLAB_API_URL/environments/18565656" | jq -r '.last_deployment.deployable.id')


# Check if website is available
if curl --output /dev/null --silent --head --fail "$WEBSITE_URL"; then
    echo "response 200"
else
    echo "404 not found, initiating rollback"
    curl --request POST --header "PRIVATE-TOKEN: $CI_PROJECT_TOKEN" "$GITLAB_API_URL/jobs/$LAST_SUCCESS_ENVIRONMENT/retry"  
fi

