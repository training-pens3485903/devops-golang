#!/bin/sh

if curl --output /dev/null --silent --head --fail "http://20.55.105.46:8080"; then
    echo "Website appears to be available"
else
    echo "Website not available, initiating rollback"

    LAST_SUCCESS_PIPELINE=$(curl -k --silent --header "PRIVATE-TOKEN: glpat-2Xu7nQyzRyUqYLa68BaZ $CI_JOB_TOKEN" "https://gitlab.example.com/api/v4/projects/53896678/pipelines?status=success&scope=finished" | jq '.[0].id')
    if [ -z "$LAST_SUCCESS_PIPELINE" ]; then
        echo "Cannot get last pipeline id"
        exit 1
    fi
    echo "Rolling back to pipeline id $LAST_SUCCESS_PIPELINE"
    
    # Change name of job name in the line below
    RETRY_JOB_ID=$(curl -k --silent --header "PRIVATE-TOKEN: glpat-2Xu7nQyzRyUqYLa68BaZ $CI_JOB_TOKEN" "https://gitlab.example.com/api/v4/projects/53896678/pipelines/$LAST_SUCCESS_PIPELINE/jobs" | jq '.[] | select(.name == "deploy") | .id')
    if [ -z "$RETRY_JOB_ID" ]; then
        echo "Cannot get job of this pipeline to retry"
        exit 1
    fi
    echo "Retrying job id $RETRY_JOB_ID"
    
    curl -k --silent --request POST --header "PRIVATE-TOKEN: glpat-2Xu7nQyzRyUqYLa68BaZ $CI_JOB_TOKEN" "https://gitlab.example.com/api/v4/projects/53896678/jobs/$RETRY_JOB_ID/retry" 2>/dev/null
fi
