# Latest golang image on apline linux
FROM golang:1.18

# Work directory
WORKDIR /docker-go

# Installing dependencies
COPY go.mod ./
RUN go mod download

# Copying all the files
COPY . .

# Starting our application
CMD ["go", "run", "main.go"]

# Exposing server port
EXPOSE 80